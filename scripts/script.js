let notas = document.querySelector(".notas")
let button = document.querySelector("#button")
let input = document.querySelector("input")
let btn_media = document.querySelector("#btn_media")
let contador = 1
let total = 0


function adicionarNota(){
    let nota1 = document.createElement("p")
    if (input.value != "" && !isNaN(input.value) && (parseFloat(input.value) >= 0 && parseFloat(input.value) <= 10)){
        nota1.classList.add("block")
        nota1.append(input.value)
        total += parseFloat(input.value)

        nota1.innerHTML = "A nota " + contador + " foi " + input.value
        nota1.classList.add("text")
        notas.append(nota1)
        input.value = ""
        contador++
    }else if(input.value == ""){
        alert("Por favor, insira uma nota")
        input.value = ""
    }else if(!(parseFloat(input.value) >= 0 && parseFloat(input.value) <= 10) && !isNaN(input.value)){
        console.log(!isNaN(input.value))
        alert("A nota digitada é inválida, por favor, insira uma nota válida de 0 a 10")
        input.value = ""
    }else{
        alert("A nota digitada é inválida, por favor, insira uma nota válida")
        input.value = ""
    }
    
}

function calculaMedia(){
    let media = document.querySelector("#media")
    media.classList.add("pts")
    console.log(total, contador)
    media.innerHTML = total / (contador -1)

}

button.addEventListener("click", adicionarNota)
btn_media.addEventListener("click", calculaMedia)


